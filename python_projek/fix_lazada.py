#!/usr/bin/env python
# encoding: utf-8
"""
fix_elevenia.py

Created by Tien M. Le on 2015-11-23.
Copyright (c) 2015 __chongiadung.com__. All rights reserved.
"""
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan, bulk
import requests
import json

COMMAND_API = "http://index.nyari.id:10001/command?command=delete"
es = Elasticsearch("http://index.nyari.id:9200")

#ES query to match, try to be restrictive as possible. avoid returning big results.
#Can do another filter below, when interating on documents
query = {"query": {"query_string": {"query": "mobapi", "fields": ["url"]}}}
INDEX_NAME = "crawled_products"
INDEX_TYPE = "product"

#Get all docs matching the query
all_docs = scan(es, query=query, index=INDEX_NAME, doc_type=INDEX_TYPE)
updatedIds = []
for doc in all_docs:
    if "lazada.co.id/mobapi" in doc['_source']['url']:
        updatedIds.append(doc['_id'])
        if len(updatedIds) % 1000 == 0:
            requests.put(COMMAND_API, data=json.dumps(updatedIds))
            print "Sent to reprocess %s docs, last processed %s" % (len(updatedIds), doc['_id'])
            updatedIds = []

if len(updatedIds) > 0:
    requests.put(COMMAND_API, data=json.dumps(updatedIds))
    print "Last Send to reprocess %s docs" % len(updatedIds)

print "DONE"
