import json, requests, sys, traceback, time

def GetDataFromApiNyari():
	start = 0
	size = 100
	totalCurrentSize = 0
	query = "gundam"
	number_of_total=0
	ApiResult = RequestGetToApiNyari(start,size,query)

	while ApiResult is not None and 'result' in ApiResult:
		for result in ApiResult['result']:
			print result['product_cluster']['id']
			CheckIdFromApiToIndexNyari(result['product_cluster']['id'])
			totalCurrentSize+=1
			time.sleep(0.3)
		start+=100
		print "\nWarning: Already read %d data\n" % (totalCurrentSize)
		ApiResult = RequestGetToApiNyari(start,size,query)

	print "\n\nWarning: Finish reading from API"

def RequestGetToApiNyari(start, size, query):
	try:
		r = requests.get("http://api.nyari.id:8888/api/Search?client=frontend&size="+str(size)+"&start="+str(start)+"&query="+query+"&category=1-16a79d4-Buku")
		if r.status_code == requests.codes.ok:
			ApiResult = r.json()
			return ApiResult
		else:
			return None
	except Exception, e:
		return None

def CheckIdFromApiToIndexNyari(cluster_id):
	try:
		r = requests.get("http://index.nyari.id/elastic/indo_item/_search?q="+cluster_id)
		if r.status_code == requests.codes.ok:
			indexResult = r.json()
			if len(indexResult['hits']['hits'])==0:
				print "Warning: This id is not exist in index.nyari.id. Writing to Log\n"
				WriteToLog("This id exist in API but not in index.nyari.id: %s\n" % (cluster_id))
		else:
			print "Error: Status code %s" % (str(r.status_code))
	except Exception,e:
		print e

def WriteToLog(words):
	file = open("ListIdNotMatch.txt","a")
	file.write(words)
	file.close

if __name__=="__main__":
	GetDataFromApiNyari()
	#CheckIdFromApiToIndexNyari("dd5697c0b1afc1bd88fcfafc61d547")

