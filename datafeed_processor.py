#!/usr/bin/python
# encoding: utf-8
"""
Base DataFeedProcessor processes data from merchant feed then save to crawled database.

Created by Giang Nguyen Truong on 2015-05-18.

Modified by Tien M.Le on 2015-05-27.

Copyright (c) 2014 Chongiadung All rights reserved.

Example of create LingoFeedProcessor:
    @item_path
        item_path = ['rss','channel','item']
    @property_map
        property_map = {
            'name' : 'title',
            'category': 'product_type',
            'images' : ['image_link', 'additional_image_link'],
            'price' : 'sale_price',
            'url' : 'link',
            'optional': {
                'promotion': 'description',
                'brand': 'brand'
            }
        }
    @name_space
        name_space = 'http://base.google.com/ns/1.0'
    @feed_type
        feed_type = FeedType.XML
"""
import requests
import json
import time
import urllib
import xmltodict
import xml
import sys
from urlparse import urlparse
from common import util
from common.logger import logging
from urllib import FancyURLopener
from kafka.client import KafkaClient
from kafka.producer import SimpleProducer

KAFKA_BROKER = 'index.nyari.id:9092'
KAFKA_TOPIC = 'indo_crawled_products'
KAFKA_MAX_SIZE = 1000
KAFKA_MAX_TIME = 5

class FeedType():
    XML = "xml"
    JSON = "json"
    
class MyOpener(FancyURLopener):
    version = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"

class DataFeedProcessor(object):
    def __init__(self, source, item_path=[], property_map={}, name_space=None, 
                 feed_type=FeedType.XML, batch_size=1000, trial=False):
        self.source = source
        self.itemPath = item_path
        self.propertyMap = property_map
        self.nameSpace = name_space
        self.batchSize = batch_size
        self.feedType = feed_type
        self.num_of_items = 0
        self.kafka = None
        self.trial = trial
            
    def setKafka(self):
           
        client = KafkaClient(KAFKA_BROKER)
        self.kafka = SimpleProducer(client,req_acks=SimpleProducer.ACK_AFTER_LOCAL_WRITE,batch_send=True,batch_send_every_n=KAFKA_MAX_SIZE,batch_send_every_t=KAFKA_MAX_TIME)

    def getFeedFromUrl(self, url , https):
        file_name = "D:\MyDataFeed\%s-%s.%s" % (self.source, time.strftime("%Y%m%d-%H%M"), self.feedType)
        logging.info("Start retrieving feed")
        if https:
            myopener = MyOpener()
            data = myopener.open(url)
            f = open(file_name, 'w')
            f.write(data.read())
            f.close()
        else:
            urllib.urlretrieve(url, file_name)
        logging.info("End retrieving feed")
        return file_name

    def parseFeedToJson(self, feed_file):
        """ Read and Parse Json or XML file and return Array of Json formated item"""
        if self.feedType == FeedType.JSON:
            return readFromJsonFile(feed_file)
        return readFromXMLFile(feed_file, self.itemPath, self.nameSpace)

    def process(self, items):
        products = []
        self.num_of_items = 0
        for item in items:
            self.num_of_items += 1
            product = {}
            product['_id'] = self.getProductId(item)
            product['name'] = self.getProductName(item)
            if product['name'] and len(product['name']) > 1:  
                product['feed'] = 1  
                product['category'] = self.getProductCategory(item)
                product['source'] = self.getSource(item)
                product['price'] = self.getProductPrice(item)
                product['url'] = self.getProductUrl(item)
                product['images'] = self.getProductImages(item)                
                product['timestamp'] = time.time()
                if "optional" in self.propertyMap:
                    for prop, prop_map in self.propertyMap['optional'].items():
                        property_value = self.getProductOptionalProperty(prop_map, item)
                        if property_value is not None: product[prop] = property_value
                products.append(product)
                if len(products) >= self.batchSize:
                    self.saveProducts(products)
                    logging.info("Processed %d items, last processed item %s saved to %s", self.num_of_items, product['_id'],KAFKA_TOPIC)
                    products = []

        if len(products) > 0:
            #self.saveProducts(products)
            logging.info("Processed %d items, last batch done!", self.num_of_items)
            file = open("result.txt", "a")
            file.write("Processed %d items, last batch done in %s!\n" % (self.num_of_items, product['source']))
            file.close()
        
        if self.trial and len(products) > 0:
            logging.info(json.dumps(products[0]))

    def saveProducts(self, products):
        if self.kafka:
            self.kafka.send_messages(KAFKA_TOPIC, *[json.dumps(product) for product in products])
            time.sleep(5)        
    
    def execute(self, feed_url, https):
        feed_file = self.getFeedFromUrl(feed_url, https)
        logging.info("Start parsing feed to Json Object")
        start = time.time()
        items = self.parseFeedToJson(feed_file)
        elapsed_time =  time.time() - start
        logging.info("Parsed to Json Object %d items. Time %d", len(items), elapsed_time)
        
        self.process(items)
      
    def done(self):
        if self.kafka:
            self.kafka.stop()
            self.kafka.client.close()
        logging.info("Done processing feed from %s" % self.source)
               
    def run(self, feed_url, trial=False, https=False):
        self.trial = trial
        if not trial:
            self.setKafka()
        self.execute(feed_url, https)
        self.done()
        
    def getProductId(self, item):
        return util.getIdFromUrl(self.getProductUrl(item))
    
    def getProductName(self, item):
        if item[self.propertyMap['name']] is not None and item[self.propertyMap['name']] != "":
            return item[self.propertyMap['name']]
    
    def getProductCategory(self, item):
        """ Must be overrided """
        raise "Not implemented Category Processing Function!"
    
    def getProductPrice(self, item):
        price = []
        price_map = self.propertyMap['price']
        if price_map in item and item[price_map]:
            price = item[price_map]
        if 'price_sp' in item and item['price_sp'] and item['price_sp'] != "" and item['price_sp'] != '0':
            price = item['price_sp']            
        return [str(price)]
    
    def getProductUrl(self, item):
        return item[self.propertyMap['url']]
    
    def getProductImages(self, item):
        image_urls = []
        for image_map in self.propertyMap['images']:
            if image_map in item and item[image_map]:
                if type(item[image_map]) == list:
                    image_urls.extend(item[image_map])
                else:
                    image_urls.append(item[image_map])
            else:
                continue
        return [url for url in set(image_urls)]
    
    def getProductOptionalProperty(self, prop_map, item):
        if prop_map in item and item[prop_map]:
            return item[prop_map]
        return None

    def getSource(self, item):
        label = 'source'
        if label in self.propertyMap and item[self.propertyMap[label]] is not None and item[self.propertyMap[label]] != "":
            url=item[self.propertyMap[label]]
            urlParseResult = urlparse(url)
            return urlParseResult.netloc
        return self.source
 
def readFromJsonFile(file_name):
    # TODO(giang): maybe rewrite this function for generalization
    data = readFile(file_name)
    return json.loads(data)

def readFromXMLFile(file_name, item_path=[], name_space=None):
    data = readFile(file_name)  
    try:  
        orderedDictData = xmltodict.parse(data, process_namespaces=True, namespaces={name_space: None})
    except xmltodict.expat.ExpatError:
        print data
        pass
    orderedItems = orderedDictData
    for path in item_path:
        orderedItems = orderedItems[path]
    items = []
    for item in orderedItems:
        items.append(dict(item))
    return items

def readFile(file_name):
    f = open(file_name, "r")
    return f.read()    
   
def main():
    pass

if __name__ == '__main__':
    main()     
