#!/usr/bin/python
# encoding: utf-8
"""
Data feed using parameter from command line argument.
"""
import sys
import json
import time
import requests
import xml
import feedparser
from datafeed_processor import DataFeedProcessor
import xml.etree.ElementTree as ET
CONFIG = {
    'item_path' : "",
    'name_space':'',
    'FeedType':'XML',
    'property_map' : {
            'name' : 'title',
            'category': ['category','scategory','sscategory'],
            'images' : ['image','image2', 'image3', 'image4', 'image5','image6', 
                        'image7', 'image8', 'image9','image10', 'image11', 'image12', 'image13'],
            'price' : 'price',
            'url' : 'link',
            'source': 'store',
            'optional': {
                'description': 'description',
                'origin_price' : 'price'
            }
        }
}

class FeedJejualan(DataFeedProcessor):
    def __init__(self,source, trial):
        DataFeedProcessor.__init__(self, source, CONFIG['item_path'],
            CONFIG['property_map'], CONFIG['name_space'], CONFIG['FeedType'], trial=trial)

    def getProductCategory(self, item):
        categoryLabels = self.propertyMap['category']
        categories = []
        for categoryLabel in categoryLabels:
            if categoryLabel in item and item[categoryLabel] is not None and item[categoryLabel] != "":
                categories.append( item[categoryLabel])
        if len(categories) > 0:
            return categories

    def parseFeedToJson(self, feed_file):
        tree = ET.parse(feed_file)
        root = tree.getroot()
        itemTag = root[0][3] # get the <item>
        #incorrect feeds format, rather than per item
        #all products same XML level <title>...</title>[...]<title></title>
        #split by actual item
        itemList = []
        currentItem = []
        for line in itemTag:
            if (line.tag == "title"):
                if len(currentItem) <> 0:
                    itemList.append(dict(currentItem))
                    currentItem = []
            currentItem.append((line.tag, line.text))
        if len(currentItem) <> 0:
            itemList.append(dict(currentItem))
        return itemList

# End class
###
# Start command

def feedOneShop(feeder, url):
    print url
    try:
        r = requests.get(url,verify=False)
        fileName = feeder.getFeedFromUrl(url, https=False);
        itemList = feeder.parseFeedToJson(fileName);
        if len(itemList) > 0:
            urlLink = itemList[0]['link']
            r = requests.get(urlLink,verify=False)
            if r.status_code != requests.codes.ok:
                writeToFile("%s link is dead or not found!\n" % (urlLink))
                printStatusResult("Warning: this link is dead or not found")
            else:
                if len(r.text)<3000:
                    writeToFile("%s link is in maintenance or under development!\n" % (urlLink))
                    printStatusResult("Warning: this link is in maintenance or under development")
                else:
                    feeder.process(itemList);
    except:
        writeToFile("%s link is dead or not found!\n" % (url))
        printStatusResult("Warning: this link is dead or not found")
    

def writeToFile(words):
    file = open("whitelist.txt","a")
    file.write(words)
    file.close
    
def printStatusResult(words):
    print(words)

if __name__ == '__main__':
    trial=False
    feeder = FeedJejualan(None, trial=trial);
    if not feeder.trial:
        feeder.setKafka();
    feedOneShop(feeder,sys.argv[1])
    #example one off : 
    #feedOneShop(feeder,'http://joseon.id/rss_pricearea.php')
    #sys.exit(0)
    
    #find all the links
    #mainurl ='https://jejualan.com/xml_merchant_aktif.php'
    #data = requests.get(mainurl)
    #tree = ET.ElementTree(ET.fromstring(data.text))
    #root = tree.getroot()
    #merchant_active = root[0][3] # get the <merchant_active>
    #for link in merchant_active:
    #    try:
    #        feedOneShop(feeder,link.text)
    #    except:
    #        print "Unexpected error: %s", sys.exc_info()[0]
    #feeder.done();
