#!/usr/bin/env python
# encoding: utf-8
"""
generate_gtoken.py

Created by Toan Vinh Luu on 2015-06-08.
Copyright (c) 2015 __local.ch AG__. All rights reserved.
"""

from oauth2client.client import OAuth2WebServerFlow
import httplib2
from apiclient.discovery import build
import gdata.analytics.client


CLIENT_ID = '393674523557-ahpoi1s9h0kcg35l5097i52s4dabe281.apps.googleusercontent.com'
CLIENT_SECRET = 'nlie8AJ446NP_8ad_zee04oU'
SCOPE = 'https://www.google.com/analytics/feeds/'  # Default scope for analytics
USER_AGENT = ''
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'
# 
SOURCE_APP_NAME = 'chongiadung.com'
TABLE_ID = 'ga:59941453'

# flow = OAuth2WebServerFlow(client_id = CLIENT_ID,
#                            client_secret = CLIENT_SECRET,
#                            scope = SCOPE,
#                            redirect_uri = REDIRECT_URI)
# 
# auth_uri = flow.step1_get_authorize_url()
# print auth_uri
# credentials = flow.step2_exchange('4/28O4agG9ONVN4xXzEDVzc1xlN4cmNTyCkzPO5WZagME.MkSYuy7M4oYayjz_MlCJoi1uVNCmmwI')
# http = httplib2.Http()
# http = credentials.authorize(http)
# service = build('analytics', 'v3', http=http)
# 
token = gdata.gauth.OAuth2Token(client_id = CLIENT_ID,
  client_secret = CLIENT_SECRET,
  scope = SCOPE,
  user_agent = USER_AGENT
 )
print  token.generate_authorize_url(redirect_uri = REDIRECT_URI)
code = '4/iYIl5vOde2-BtTYlaOzF4dShBa9IYr22GjmM4pa9oy4.YlvGitIo6g4aYFZr95uygvW7MPummwI'
 
# # code = 'random-string-from-redirected-page'
token.get_access_token(code) 
# 
analytics_client = gdata.analytics.client.AnalyticsClient(source=SOURCE_APP_NAME)
token.authorize(analytics_client)
# 
# # 
data_query = gdata.analytics.client.DataFeedQuery({
    'ids': TABLE_ID,
    'start-date': '2015-03-01',
    'end-date': '2015-03-30',
    'dimensions': 'ga:eventAction,ga:eventLabel',
    'metrics': 'ga:totalEvents',
    'filters': 'ga:eventAction==has_result',
    'sort': '-ga:totalEvents',
    'max-results': 100})
# 
feed = analytics_client.GetDataFeed(data_query)

topsearch = {}
for entry in feed.entry:
    # key = urllib.unquote(entry.dimension[1].value)
    key = entry.dimension[1].value
    value = int(entry.metric[0].value)
    print key, value


# service.data().ga().get(
#         ids='ga:' + PROFILE_ID,
#         start_date=from_date,
#         end_date=to_date,
#         metrics='ga:pageviews',
#         dimensions='ga:pagePath,ga:date',
#         filters='ga:pagePath=@docid=363746',
#         max_results=MAX_RESULTS,
#         start_index=start_ind).execute()