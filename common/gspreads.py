#!/usr/bin/env python
# encoding: utf-8
"""
gspread.py

Created by Toan Vinh Luu on 2014-03-27.
Copyright (c) 2014 __local.ch AG__. All rights reserved.
"""

import gspread
from oauth2client.client import SignedJwtAssertionCredentials

USERNAME = 'chongiadung.vn@gmail.com'
PASSWORD = 'glagxggfxpvzphnh'
TESTKEY = '1qMr0MQiYbvz26L2nThqG46mUgUS-CrGMfZ3VFCFpDJU'

CLIENT_EMAIL = "1093127782126-t5o7d5ek0s60hrohd8cp3m2jcjlgqn11@developer.gserviceaccount.com"
CLIENT_KEY = u"-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDVyHxI2XkNrPOG\nuBnQgJ8PsUOJn1WO1Nzh2EkLn1Hre2kV4Oq+tRh1TPV+2SXc+T0xI/sSD5zuGqFh\nv6zWeEz2GljvUXpz0AsfhDvSP2FNf3qT2wa94i0ziEKB9PsLzrh2rGGk8dAsNEPm\n5cO0DZulEUBWgqJVc1vlQKYTUsy1zkzTjw6joxLJu837mVao86EH1pXnz4uFFHbc\ncQ/SpMVKZLoRFm/qnt/k6NdniNuRceZVOegV11VM8Kggg69y937kcbkkGXwOJcpF\nT6QTVPMDrV65v9WLJtDm/NSTw20ffw5e4vO2VAJdEcL3Mh2KUmuRYleVTddDUr/1\nOU2JkxFpAgMBAAECggEAM8wyyNfga77W9nJO1uUDJSM3EPZJHaVXq2TjNeyxRZQO\nPx3S4sgwF+PAq65jbSn6eJiWMacUx+At7+goMYLOauaxW7LrL3pd55H7iJKQud1E\nTuTHumQ9vXmrW5+G4DKDIUXdFsD8RmmXdi9/fnW5m4IVi6B1Qq0DvAUw+Aec0AOt\n3fWutxYHiprvJBqLdoVCjHtnWe+0oLYCy0ycx/UMKePu4r3uci1vd3JbfafIrNAU\nMplq5CuuMOb6NlLMMCjYimf6YqcJC69H5WnRODm1AnUm12pmyRQFmfw+ZdyPOobP\nO2uA2V7nTHLscZcO/gDV3yJtn2Brnrxb01olpfORAQKBgQDqzY+n3UuYCdnO1+9E\n4/JmsY3ydrEtI4p5HwWP8poRIsRXDCzxts2/1NG7Y2Tytf6utL/CPb6bxpj/bzom\nAYckeSIp0NGe26Ym1mHAh/GsiPzKZcbZZyBIHZ6fBA6GMeAJkxHKG+feooa28ZTb\nTZvwjOeGw1WcJNoh89BDEUCi4QKBgQDpFSS2fDu9WudOChV7C24ImuY/wSm93Uv2\nRT2VPl+6koNnN1QKK9KEeNp1s8Sfq5JpvS4sZXHCn8/1efTnmIizvycMgHrUj3fs\nOo3Cfm1Moprj/lAaw7+egLFgREG5wNFW0h/+j0lBznddI6AN+AlExIkOJrCNjEME\n1fZROj/HiQKBgHgGnmGj6MmOd42ZqlDf6QB/0NzZuZYmbdmedvxvejw6Nn181ti0\nppZiFtggZfEAAXGTKdcEMxR0P6+LTwEwlMGbPlB70zvOci+2NP/zPiIBYJ+hyTVG\n1ySmJ3iH3VjR1AFw8Y4MF8oh8JVCBuLX2Fft30hvP6ry16K1fIdcIVthAoGAQP6/\nXQ+pjPTjWTSgps4aIdYn7JFNtX8XJpZKROq+wQUIFWktSyTLMFuO95Foe+nZCxlQ\n3dpv7Yio8dfPPiGfxRfBqM/O8KUNGNd/HkQo8FzLelY5Oi1pqqZST/jYpxaoiWBQ\nwahtEiejy1oa0Kpg00dGvKZ3RxFcwuVzKVVn4ZkCgYEA36t+acO+OFk7lKXLKj1g\nYvBA+zEdXc5FMiMF46QkeNcXd1uT/kDTbUtpgbiDV1X6p9+jcO7iXzhfFis+U3Yy\nP4m6Bnwso1cTtMrMXCNC6PpkJ4jxlERtPRNs6dJX5ufZF/yHGgBgZBqDxY1xOphC\nQX0lFHYuVvRytZN6oSxmI5I\u003d\n-----END PRIVATE KEY-----\n"

def getWorksheet(key, wsNumOrTitle):
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = SignedJwtAssertionCredentials(CLIENT_EMAIL, CLIENT_KEY, scope)
    gc = gspread.authorize(credentials)
    sh = gc.open_by_key(key)
    if type(wsNumOrTitle) == int:
        return sh.get_worksheet(wsNumOrTitle)
    else:
        return sh.worksheet(wsNumOrTitle)
    
def updateTable(ws, list_of_list):
    numRows = len(list_of_list)
    numCols = 0
    for row in list_of_list:
        if len(row) > numCols: numCols = len(row)
    
    if  ws.row_count < numRows: ws.resize(rows = numRows)
    if  ws.col_count < numCols: ws.resize(cols = numCols)
    
    print 'Update table with rows =', numRows, ', cols =', numCols
    leftbottom = str(unichr(65 + numCols - 1)) + str(numRows)
    
    cells = ws.range('A1:' + leftbottom)
    for cell in cells:
        cell.value = list_of_list[cell.row - 1][cell.col - 1]
    ws.update_cells(cells)
    
def updateRow(key, ws_num, row_num, alist):
    ws = getWorksheet(key, ws_num)
    print 'Update row', row_num
    for col in range(len(alist)):
        ws.update_cell(row_num, col + 1, alist[col])
    print 'Done. View at https://docs.google.com/spreadsheet/ccc?key=' + key + '#gid=' + str(ws_num)
    
def readTable(key, ws_num, numRows, numCols):
    ws = getWorksheet(key, ws_num)
    leftbottom = str(unichr(65 + numCols - 1)) + str(numRows)
    cells = ws.range('A1:' + leftbottom)
    results = []
    for row in range(0, numRows):
        rows = []
        results.append(rows)
        for col in range(0, numCols):
            rows.append('')
            
    for cell in cells:
        results[cell.row - 1][cell.col - 1] = cell.value
    return results
    
def example1():
    #prepare data
    cells = []
    cells.append(['', 'l1', 'l2', 'l3'])
    for row in range(0,30):
        rows = ['day ' + str(row + 1)]
        for col in range(0,3):
            rows.append(row + col)
        cells.append(rows)
    #update:        
    updateTable(TESTKEY, 0, cells)

def example2():      
    table = readTable(TESTKEY, 0, numRows = 50, numCols = 3)
    for row in table:
        print row
        

def example3():
    #prepare data
    cells = []
    cells.append(['', 'l1', 'l2', 'l3'])
    for row in range(0,30):
        rows = ['day ' + str(row + 1)]
        for col in range(0,3):
            rows.append(row + col)
        cells.append(rows)
    #update:        
    updateTable(TESTKEY, 1, cells)
        
    
def main():
    example2()
    # updateRow(TESTKEY, 0, 10, ['newValue1', 'newValue2', 'newValue3'])
    # example2()
    # example3()

if __name__ == '__main__':
    main()



