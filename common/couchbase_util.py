#!/usr/bin/env python
# encoding: utf-8
"""
common.couchbase_util
Utilities for using CouchBase

Created by Tien M. Le on May 10, 2015.
Copyright (c) 2014 __ChonGiaDung.com CGD__. All rights reserved.
"""
import json
from common.logger import logging
from couchbase.bucket import Bucket
from couchbase.exceptions import NotFoundError, HTTPError, KeyExistsError, TemporaryFailError
from couchbase.items import Item

from common import couch_util
import time

LOGGER = logging.getLogger("common.couchbase_util")

def getDb(server, bucket_name, timeout=10.0, lockmode=1):
    bucket =  Bucket(server + bucket_name, lockmode=lockmode)
    bucket.timeout = timeout
    return bucket

def createDb(name,
             user="tc", passwd="123456",
             ram=100,
             replica=0,
             server="http://db.chongiadung.com:8091/pools/default/buckets"):
    """ Create a new bucket by using system curl command
    """
    # curl -X POST -u username:password -d name=newbucket -d ramQuotaMB=100 -d authType=none
    # -d replicaNumber=1 -d proxyPort=11216 http://localhost:8091/pools/default/buckets
    command = "curl -X POST -u %s:%s -d name=%s -d ramQuotaMB=%s -d authType=sasl " \
                    "-d replicaNumber=%s %s" \
                    % (user, passwd, name, ram, replica, server)
    import commands
    _, output = commands.getstatusoutput(command)
    lines = output.split("\n")
    if len(lines) < 4:
        logging.info("Create new bucket: %s" % name)
        return True
    response = json.loads(lines[3])
    if 'errors' in response:
        logging.error(response)
        return False
    else:
        logging.info("Create new bucket: %s" % name)
        return True

def getDoc(db, docid):
    try:
        doc = db.get(docid)
        return doc.value
    except NotFoundError:
        return None

def getKeysAndDocsByIds(db, docids):
    rows = db.get_multi(docids, quiet=True)
    for row in rows.values():
        yield row.key, row.value

def getDocsByIds(db, docids):
    rows = db.get_multi(docids, quiet=True)
    for row in rows.values():
        yield row.value

def createOrUpdate(db, docid, doc):
    try:
        return db.upsert(docid, doc)
    except KeyExistsError:
        logging.warning("Locking currently %s", docid)
        return None

def createOrUpdateBatch(db, doc_batch):
    assert type(doc_batch) == list, "Bad input %s" % type(doc_batch)
    assert '_id' in doc_batch[0], "Bad format doc, must have `_id` field"

    logging.warning("createOrUpdateBatch() by list of docs is deprecated, please " +
                    "use upsertBatch with dict of docs.")
    batch = {}
    for doc in doc_batch:
        batch[doc['_id']] = doc
    return upsert_batch(db, batch)

def upsert_batch(db, doc_batch):
    assert type(doc_batch) == dict, "Bad input %s" % type(doc_batch)
    failed_records = {}
    responses = {}
    try:
        responses = db.upsert_multi(doc_batch)
    except TemporaryFailError as err:
        for key, res in err.all_results.items():
            if res.success:
                responses[key] = res
            else:
                failed_records[key] = doc_batch[key]
    num_failed = 0
    if len(failed_records) > 0:
        num_of_tries = 1
        max_tries = 3
        # retry upsert after sleep time to server resumes memory
        while num_of_tries <= max_tries:
            sleep_time = max(len(failed_records) * num_of_tries / 100, 1.0)
            logging.warning(" --- sleep %s seconds to retry upserting %d records", sleep_time, len(failed_records))
            time.sleep(sleep_time)
            try:
                responses.update(db.upsert_multi(failed_records))
                break
            except TemporaryFailError as err:
                failed_records = {}
                num_of_tries += 1
                for key, res in err.all_results.items():
                    if res.success:
                        responses[key] = res
                    else:
                        if num_of_tries > max_tries:
                            logging.error("Cannot upsert %s", key)
                            num_failed += 1
                            continue
                        logging.warning(" --- %d times retry upserting %s", num_of_tries, key)
                        failed_records[key] = doc_batch[key]

    if num_failed:
        logging.error("%d failed out of %d upserted.", num_failed, len(responses))
    return responses

def delete(db, docid):
    ok = db.remove(docid, quiet=True)
    if not ok.success:
        return None
    return docid

def deleteDocsByIds(db, docids):
    assert type(docids) == list, "Bad input %s" % type(docids)
    logging.warning("deleteDocsByIds() is deprecated, please use delete_batch(db, keys)")

def delete_batch(db, docids):
    assert type(docids) == list, "Bad input %s" % type(docids)
    oks = db.remove_multi(docids, quiet=True)
    deleted_keys = []
    failed_delete = 0
    for docid in docids:
        if oks[docid].rc == 0xD:
            failed_delete += 1
            logging.error("Not found key %s to delete", docid)
        else:
            deleted_keys.append(docid)
    if failed_delete:
        logging.error("Delete fail %d out of %d docs", failed_delete, len(deleted_keys))
    return deleted_keys

def deleteAllDocs(db):
    bulk = 10000
    docids = []
    for key in get_ids_pager(db):
        docids.append(key)
        if len(docids) >= bulk:
            deleteDocsByIds(db, docids)
            docids = []
    if len(docids) > 0:
        deleteDocsByIds(db, docids)

def get_pager(db, design="doc", view_name="_all_docs", startkey=None, startkey_docid=None, endkey=None, endkey_docid=None, bulk=10000, include_docs=True):
    """ Iterate over docs of db by bulk
    """
    options = {'limit': bulk}
    if startkey:
        options['startkey'] = startkey
        if startkey_docid:
            options['startkey_docid'] = startkey_docid
    if endkey:
        options['endkey'] = endkey
        if endkey_docid:
            options['endkey_docid'] = endkey_docid
    options['include_docs'] = include_docs
    options['stale'] = False
    options['connection_timeout'] = 600000
    done = False
    try:
        while not done:
            rows = db.query(design, view_name, **options)
            cnt = 0
            for row in rows:
                cnt += 1
                options['startkey'] = row.key
                options['startkey_docid'] = row.docid
                options['skip'] = 1
                yield row.doc.value
            if cnt < bulk:
                done = True
    except HTTPError:
        logging.error("_all_docs design has not exists. Please use function design_doc to create.")
        raise HTTPError

def get_ids_pager(db, design="doc", view_name="_all_docs", startkey=None, startkey_docid=None, endkey=None, endkey_docid=None, bulk=10000, include_docs=False):
    """ Iterate over docs of db by bulk
    """
    options = {'limit': bulk}
    if startkey:
        options['startkey'] = startkey
        if startkey_docid:
            options['startkey_docid'] = startkey_docid
    if endkey:
        options['endkey'] = endkey
        if endkey_docid:
            options['endkey_docid'] = endkey_docid
    options['include_docs'] = include_docs
    options['full_set'] = True
    done = False
    try:
        while not done:
            rows = db.query(design, view_name, **options)
            cnt = 0
            for row in rows:
                cnt += 1
                options['startkey'] = row.key
                options['startkey_docid'] = row.docid
                options['skip'] = 1
                yield row.docid
            if cnt < bulk:
                done = True
    except HTTPError:
        logging.error("_all_docs design has not exists. Please use function design_doc to create.")
        raise HTTPError

def design_doc(db, design="doc", name="_all_docs"):
    doc_by_all_docs = {
        'map': '''
        function(doc, meta) {
            emit(null, null);
        }
    '''
    }

    doc_design = {
        'views': {
            '_all_docs' : doc_by_all_docs
        }
    }
    bucket_manager = db.bucket_manager()
    try:
        doc_design = bucket_manager.design_get("doc", use_devmode=False)
        if '_all_docs' not in doc_design.value['views']:
            doc_design.value['views']['_all_docs'] = doc_by_all_docs
            bucket_manager.design_create("doc", doc_design.value, use_devmode=False, syncwait=5)
    except HTTPError:
        bucket_manager.design_create("doc", doc_design, use_devmode=False, syncwait=5)

def copy_database(fromDb, toDb, batch_size=10000):
    doc_batch = {}
    cnt = 0
    for doc in get_pager(fromDb):
        doc_id = doc['_id']
        doc_batch[doc_id] = doc
        if len(doc_batch) >= batch_size:
            cnt += len(doc_batch)
            upsert_batch(toDb, doc_batch)
            logging.info("Copied %d records" % cnt)
            doc_batch = {}

    if len(doc_batch) > 0:
        cnt += len(doc_batch)
        upsert_batch(toDb, doc_batch)
        logging.info("Copied %s records" % cnt)

    logging.info("Copied %d records from %s to %s. DONE!" % (cnt, fromDb.bucket, toDb.bucket))

def copy_couchdb_to_couchbase(fromDb, toDb, batch_size=10000):
    doc_batch = {}
    cnt = 0
    for doc in couch_util.get_pager(fromDb):
        del doc['_rev']
        doc_batch[doc['_id']] = doc
        if len(doc_batch) >= batch_size:
            cnt += len(doc_batch)
            upsert_batch(toDb, doc_batch)
            logging.info("Copied %s docs" % cnt)
            doc_batch = {}

    if len(doc_batch) > 0:
        cnt += len(doc_batch)
        upsert_batch(toDb, doc_batch)
        logging.info("Copied %s docs" % cnt)
    logging.info("Copied %d records from CouchDb %s to Couchbase %s. DONE!" % (cnt, fromDb.name, toDb.bucket))

def copy_couchbase_to_kafka(fromDb, kafka_producer, kafka_topic, batch_size=1000):
    batch = []
    for doc in get_pager(fromDb):
        batch.append(doc)
        if len(batch) > batch_size:
            kafka_producer.send_messages(kafka_topic, *[json.dumps(msg) for msg in batch])
            batch = []
    if len(batch) > 0:
        kafka_producer.send_messages(kafka_topic, *[json.dumps(msg) for msg in batch])
    logging.info("Saving Couchbase Bucket %s -> Kafka %s. DONE!" % (fromDb.bucket, kafka_topic))

def copy_couchbase_to_kafka_default(fromDbName, kafka_topic, batch_size=1000):
    fromDb = getDb("couchbase://db.chongiadung.com/", fromDbName)
    from kafka.client import KafkaClient
    from kafka.producer import SimpleProducer
    kafka_client = KafkaClient("index02.chongiadung.com:9092,index.chongiadung.com:9092")
    kafka_producer = SimpleProducer(kafka_client,
                             req_acks=SimpleProducer.ACK_AFTER_LOCAL_WRITE,
                             batch_send=True,
                             batch_send_every_n=batch_size,
                             batch_send_every_t=5
                            )
    copy_couchbase_to_kafka(fromDb, kafka_producer, kafka_topic, batch_size=batch_size)

class CouchbaseOperationType():
    DELETE = 1
    UPSERT = 0

class CouchbaseRecord(Item):
    def __init__(self, key=None, value=None, cas=0, op_type=CouchbaseOperationType.UPSERT):
        Item.__init__(self, key, value)
        self.cas = cas
        self.op_type = op_type

def createCouchbaseRecordFromValueResult(result):
    record = CouchbaseRecord()
    record.key = result.key
    record.value = result.value
    record.cas = result.cas
    return record

def main():
    pass

if __name__ == '__main__':
    main()
