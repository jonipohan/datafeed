#!/usr/bin/env python
# encoding: utf-8
"""
util_xtract.py

Created by Toan Vinh Luu on 2015-07-10.
Copyright (c) 2015 __local.ch AG__. All rights reserved.
"""

import sys
import os
import requests
from lxml import html
import urllib2

def xtract(url, xpath):
    tree = get_tree(url)
    return tree.xpath(xpath)

def xtract_fields(url, field_xpaths):
    tree = get_tree(url)
    return xtract_fields_from_tree(tree, field_xpaths)

def get_tree(url):
    page = requests.get(url)
    tree = html.fromstring(page.text)
    return tree
    
def xtract_fields_from_tree(tree, field_xpaths):
    results = {}    
    for k, v in field_xpaths.items():
        values =  tree.xpath(v)
        results[k] = values

    return results

def xtract_from_tree(tree, xpath):
    return tree.xpath(xpath)        

def main():
    # print xtract('http://websosanh.vn/thoi-trang-my-pham/cat-3.htm','.//div[@data-fid="com"]/div/select/option/text()')
    print xtract('https://www.chodientu.vn/model/dien-thoai-di-dong?trang=6','.//div[@class="model-item"]/div[@class="item-link"]/a/@href')
if __name__ == '__main__':
    main()

