#!/usr/bin/env python
# encoding: utf-8
"""
multi_processors.py

1 reader and multi processors, use queue to store data.

Created by Toan Vinh Luu on 2014-12-18.
Copyright (c) 2014 __local.ch AG__. All rights reserved.
"""

import sys
import os
import threading
import Queue
import time
import util_dt as udt

#END object, used to detected that no data to process anymore in the queue
END = '_END_'
#maximum queue size
QUEUE_SIZE = 100000


class Processor(object):
    
    def __init__(self, _id):
        self._id = _id
        self.cnt = 0
        
    def process(self, data):
        print 'Need to implement!'

    def done(self):
        print udt.timestamp(), 'Processor', self._id, 'done with', self.cnt
        
    def count(self, step = 1000):
        self.cnt += 1
        if self.cnt % step == 0:
            print udt.timestamp(), '===>Processor', self._id, 'processed', self.cnt
    
    def getCount(self):
        return self.cnt
    
    def getId(self):
        return self._id


class MultiProcessors(object):
    def __init__(self):
        self.listProcessors = []
        self.numProcessor = 0
        
    def addProcessor(self, processor):
        self.listProcessors.append(processor)
        self.numProcessor  = len(self.listProcessors)
        return self.numProcessor
        
    def getAndProcess(self, processor):
        while True:
            data = self.dataQueue.get()
            if type(data) is str and data == END:
                processor.done()
                break
            else:
                processor.process(data)
            self.dataQueue.task_done()
    #read data and put to queue
    def read(self, data):
        self.dataQueue.put(data)
        
    def start(self):
        self.dataQueue = Queue.Queue(QUEUE_SIZE)
        for processor in self.listProcessors:
            t = threading.Thread(target = self.getAndProcess, args=(processor,))
            t.daemon = True
            t.start()
        self.dataQueue.join()
        
    def done(self):
        for i in range(0, self.numProcessor):
            self.dataQueue.put(END)
        
        #sleep abit to give time to process all items
        while self.dataQueue.qsize() > self.numProcessor:
            print udt.timestamp(), 'Queue size:', self.dataQueue.qsize()
            time.sleep(5)

"""
Example of multi processors, your usercase can follow this example
"""    
class ExampleProcessor(Processor):
    #Do whatever you want with data here
    def process(self, data):
        self.count(10000)
    
    def done(self):
        super(ExampleProcessor, self).done()
        print udt.timestamp(), self.getId(), '===>I\'m done with', self.getCount()
    
class ExampleMultiProcessors(MultiProcessors):
    
    def processData(self):
        for i in range(0, 4):
            testProcessor = ExampleProcessor(i)
            self.addProcessor(testProcessor)
        self.start()
        
        for i in range(0, 123456):
            self.read(i)
        self.done()
        
    
def main():
    example = ExampleMultiProcessors()
    example.processData()

if __name__ == '__main__':
    main()

